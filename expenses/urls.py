from django.contrib import admin
from django.urls import path, include

# from .views import home_page
from django.shortcuts import redirect


def home_page(request):
    return redirect("home")


urlpatterns = [
    path("", home_page),
    path("receipts/", include("receipts.urls")),
    path("accounts/", include("accounts.urls")),
    path("admin/", admin.site.urls),
]
